package br.com.pontoservice.scheduled;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {

    private final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

    @Scheduled(cron = "${cron.entryWorks}")
    public void entryWorks() {
        log.info("[EntryWorks] Starting entryWorks");

        try {
            log.info("[EntryWorks] Starting entryWorks Foton successfully executed");
        } catch (Exception e){
            log.error("[EntryWorks] Error Foton entryWorks: {}",e.getLocalizedMessage());
        }
    }

    @Scheduled(cron = "${cron.exitLunch}")
    public void exitLunch() {
        log.info("[ExitLunch] Starting exitLunch");

        try {
            log.info("[ExitLunch] Starting exitLunch Foton successfully executed");
        } catch (Exception e){
            log.error("[ExitLunch] Error Foton exitLunch: {}",e.getLocalizedMessage());
        }
    }

    @Scheduled(cron = "${cron.entryLunch}")
    public void entryLunch() {
        log.info("[EntryLunch] Starting entryLunch");

        try {
            log.info("[EntryLunch] Starting entryLunch Foton successfully executed");
        } catch (Exception e){
            log.error("[EntryLunch] Error Foton entryLunch: {}",e.getLocalizedMessage());
        }
    }

    @Scheduled(cron = "${cron.exitWorks}")
    public void exitWorks() {
        log.info("[ExitWorks] Starting exitWorks");

        try {
            log.info("[ExitWorks] Starting exitWorks Foton successfully executed");
        } catch (Exception e){
            log.error("[ExitWorks] Error Foton exitWorks: {}",e.getLocalizedMessage());
        }
    }

}
