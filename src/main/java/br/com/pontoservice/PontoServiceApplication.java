package br.com.pontoservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EnableScheduling
public class PontoServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PontoServiceApplication.class, args);
	}

//	@Scheduled(cron = "1 * * * * ?")
//	public void scheduleTaskUsingCronExpression() {
//
//		long now = System.currentTimeMillis() / 1000;
//		System.out.println(
//				"schedule tasks using cron jobs - " + now);
//	}
}
